import React from 'react'
import { Layout } from 'antd';

import Header from './components/layout/Header'
import Footer from './components/layout/Footer'

import Home from './components/pages/Home'

import 'antd/dist/antd.css'
import './App.css'

const { Content } = Layout;

const App = () => {

    return (
        <div>
            <Layout>
                <Header />
                <Layout style={{ background: "#101921", minHeight: "100vh" }}>
                    <Home>
                    <Content style={{
                        background: "#101921",
                        padding: '0 20px',
                        marginBottom: 200,
                        marginTop: 64
                    }}>
                    </Content>
                    </Home>
                </Layout>
                <Footer />
            </Layout>
        </div>
    );
}

export default App;
