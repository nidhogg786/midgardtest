import React from 'react'
import { Layout, Row, Col} from 'antd';

import { Center, Text } from '../Components'

const Header = (props) => {

  const styles = {
    position: 'fixed',
    zIndex: 1,
    width: '100%',
    backgroundColor:"#2B3947",
  }

  return (

        <Layout.Header className="header-container" style={styles} >
          <Row>
            <Col sm={8} md={4}>
                <Text size={"20"} bold={"TRUE"}>MIDGARD API</Text>
            </Col>
            <Col sm={8} md={14}>
              <Center>
                <span><Text size={"20"} bold={"TRUE"} color={"#23DCC8"}>TESTNET</Text></span>&nbsp;
                <span><Text size={"20"} bold={"TRUE"} color={"#FFF"}>MAINNET</Text></span>
                </Center>
            </Col>
            <Col sm={8} md={6}>
            </Col>
          </Row>
      </Layout.Header>
)
}

export default Header
