import React, { useState, useEffect } from 'react'
import axios from "axios";
import byzantine from '@thorchain/byzantine-module'
var midgardPaths;


export const useMidgard = (endpoint) => {
    const blackList = ['/v1/doc', '/v1/swagger.json']

    const [state, setState] = useState({midgardPaths: midgardPaths, data: null, node: null, loading: true})

    useEffect(() => {
        setState({midgardPaths: midgardPaths, data:null, node: state.node ? state.node : null, loading: true})

        const isDirty = (object, path) => {
            return (!blackList.find(e => e === path) && !object[path].get.parameters) ? false : true;
        }

        const fetchMidgardPaths = async() => {
            const baseUrl = await byzantine()
            setState({node: baseUrl})
            let URL = `${baseUrl}/v1/swagger.json`;
            let res = await axios.get(`${URL}`)
            let data = res.data.paths
            let allPaths = Object.keys(data).map(key => key)
            midgardPaths = allPaths.filter(path => !isDirty(data, path));
        }

        const fetchMidgard = async() => {
            if(!midgardPaths){await fetchMidgardPaths()}
            const baseUrl = await byzantine()
            let URL = `${baseUrl}${endpoint}`
            axios.get(`${URL}`)
                .then(res => {
                    console.log(endpoint)
                    let staker = endpoint && endpoint.includes('/v1/stakers/') ? endpoint.split('/')[3] : null
                    setState({midgardPaths: midgardPaths, data: JSON.stringify(res.data,null,4), currentStaker: staker, node: baseUrl, prevPath: state.currentPath, currentPath: endpoint, loading: false})
                })
                .catch(err => {
                    setState({midgardPaths: midgardPaths, data: 'Error fetching data...' , node: baseUrl, loading: false})
                })
          

            
        }

        fetchMidgard()
    
    }, [endpoint])

    return state;
}