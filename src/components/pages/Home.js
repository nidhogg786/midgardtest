import React, { useState, Fragment } from 'react'
import { Row, Col } from 'antd';

import { Click, Text, Button } from '../Components'
import { useMidgard } from './Midgard'

const paneStyles = {
    marginTop: 50,
    marginLeft: 50,
    marginRight: 50,
    marginBottom: 50,
    padding: 10,
    backgroundColor: "#2B3947",
    border: "1px solid #303942",
    borderRadius: "5px"
}

const codeStyles = {
    fontFamily: "Courier New"
}

const buttonPoolStyle = {
    fontFamily: "Open Sans",
    fontSize: "14px",
    color: "rgb(255, 255, 255)",
    letterSpacing: "0px",
    height: "40px",
    width: "250px",
    borderRadius: "9px",
    backgroundColor: "rgb(0, 0, 0)",
    border: "1px solid rgb(79, 225, 196)"
}


const buttonStakerStyle = {
    fontFamily: "Open Sans",
    fontSize: "14px",
    color: "rgb(255, 255, 255)",
    letterSpacing: "0px",
    height: "40px",
    width: "500px",
    borderRadius: "9px",
    backgroundColor: "rgb(0, 0, 0)",
    border: "1px solid rgb(79, 225, 196)"
}

const codeblock = {
        fontFamily: "Consolas, Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, serif",
        marginBottom: "10px",
        marginTop:  "25px",
        overflow: "auto",
        width: "650px!ie7",
        padding: "5px",
        backgroundColor: "#101921",
        paddingBottom: "20px!ie7",
        maxHeight: "600px"
  }

const Home = () => {
    const [endpoint, setEndpoint] = useState('/v1/health')
    const { midgardPaths, prevPath, currentPath, data, currentStaker, node, loading } = useMidgard(`${endpoint}`)

    
    const poolList = () => {
        if(currentPath === '/v1/pools'){
            let pools = JSON.parse(data)
            return pools.map(pool => {
                return (
                    <Row >
                        <Col xs={24} sm={11} md={10} lg={9}>
                            <button style={buttonPoolStyle} onClick={() => setEndpoint(`/v1/pools/detail?asset=${pool}`)}>{pool}</button>
                        </Col>
                    </Row>
                )
            }) 
        }    
    }

    const stakerList = () => {
        if(currentPath === '/v1/stakers'){
            let stakers = JSON.parse(data)
            return stakers.map(staker => {
                return (                   
                        <Row >
                            <Col xs={24} sm={11} md={10} lg={9}>
                                <button style={buttonStakerStyle} onClick={() => setEndpoint(`/v1/stakers/${staker}`)}>{staker}</button>
                            </Col>
                        </Row>
                )
            }) 
        }    
    }

    const crumbs = () => {
        if(currentPath && currentPath.includes(`/v1/stakers/${currentStaker}`) && !currentPath.includes(`/pools`)){
            return (
            <Row style={{ marginBottom: 20 }}>
            <Col xs={24} sm={11} md={10} lg={9}>
                <Click><a onClick={() => setEndpoint(`/v1/stakers`)}><h4 style={{ color: "#23DCC8" }}>/v1/stakers</h4></a></Click>
            </Col>
            </Row>
        )
            } else {
                if(currentPath && currentPath.includes(`/v1/stakers/${currentStaker}/pools`)){
                return (
                    <Row style={{ marginBottom: 20 }}>
                    <Col xs={24} sm={11} md={10} lg={9}>
                        <Click><a onClick={() => setEndpoint(`/v1/stakers/${currentStaker}`)}><h4 style={{ color: "#23DCC8" }}>{`/v1/stakers/${currentStaker}`}</h4></a></Click>
                    </Col>
                    </Row>
                )
            }
        }
    }

    const stakerPool = () => {
        console.log(currentStaker)
        if(currentPath && currentPath.includes(`/v1/stakers/${currentStaker}`)){
            if(currentPath.includes(`/pools`)){ return }
                let stakerData = JSON.parse(data)
                let stakerPools = stakerData.poolsArray

                let output = stakerPools.map(pool => {
                    return (                  
                        <Row>
                            <Col xs={24} sm={11} md={10} lg={9}>
                                <button style={buttonPoolStyle} onClick={() => setEndpoint(`/v1/stakers/${currentStaker}/pools?asset=${pool}`)}>{pool}</button>
                            </Col>
                        </Row>
                    )
                })

                return (output)
        }    
    }
    
    return (
        
        <div>
            <Row style={{ marginTop: 50 }}>
                <Col xs={24} sm={5} md={5} style={paneStyles}>

                    <Text color={"#fff"} size={18}>ENDPOINTS</Text>
                    <hr></hr>
                    <Row style={{ marginTop: 20 }}>
                    </Row>
                    {/* {console.log(midgardPaths)}
                    {console.log(data)} */}
                    {!node ?
                        'Byzantine...' :
                         !midgardPaths ?
                            'Fetching paths...' :
                             midgardPaths.map(path => {
                        return(
                            <Row >
                                <Col xs={24} sm={11} md={10} lg={9}>
                                    <Click><a onClick={() => setEndpoint(path)}><h4 style={{ color: "#23DCC8" }}>{path}</h4></a></Click>
                                </Col>
                            </Row>
                            )
                        })}
                </Col>

                <Col xs={24} sm={16} md={16} style={paneStyles}>
                <Text color={"#fff"} size={18}>RESPONSE</Text>
                <hr></hr>
                    <Row>
                    
                        <Col xs={24} sm={11} md={10} lg={9} style={codeStyles}>
                            <h4 style={{ color: "#848E9C" }}>
                                {poolList()}
                                {stakerList()}
                                {crumbs()}
                                {stakerPool()}
                                { data && data.poolsArray ? console.log(data.poolsArray) : null}
                                <blockquote>
                                    <pre style={codeblock}>
                                        <code>
                                        {loading ? '' : data}
                                        </code>
                                    </pre>
                                </blockquote>                         
                                </h4>
                        </Col>
                    </Row>

                </Col>
            </Row>

        </div>
    )
}

export default Home
